package main

import (
	"fmt"
	"net/http"
	"time"
)

//getResponse takes a url and Gets it. Panics if any error occurs.
func GetResponse(url string) {
	_, err := http.Get(url)
	if err != nil {
		panic("Error establishing connection")
	}

}

//GetAverageTime takes in the total time, interval and elapsed time and returns a float value of the average time duration
func GetAverageTime(totalTime, interval, elapsedTime time.Duration) float64 {
	t := float64(totalTime)
	i := float64(interval)

	return float64(elapsedTime) / (t / i)
}

func main() {

	url := "https://about.gitlab.com"
	var elapsedTime time.Duration
	totalTime := time.Minute * 5
	interval := time.Second * 10

	//timer is the var which tracks how many minutes it has been since the program started sending requests
	timer := time.Now()

	//code block to be executed while the timer is below 5 minutes
	for time.Since(timer) <= time.Duration(totalTime) {

		//start tracks the response time every time and adds it to the var elapsedTime
		start := time.Now()
		GetResponse(url)

		//elapsedTime adds all the response times
		elapsedTime += time.Since(start)
		fmt.Println("Response time - ", time.Since(start))
		time.Sleep(interval)
	}

	averageResponseTime := GetAverageTime(totalTime, interval, elapsedTime)

	fmt.Printf("Average Response time %v seconds\n", averageResponseTime)

}
