package main

import (
	"net/http"
	"testing"
	"time"
)

func TestGetResponse(t *testing.T) {
	url := "https://about.gitlab.com"
	_, err := http.NewRequest("GET", url, nil)

	if err != nil {
		t.Fatal(err)
	}
}

func TestGetAverageTime(t *testing.T) {
	totalTime := time.Minute * 5
	interval := time.Second * 10
	elapsedTime := time.Second * 90

	averageTime := GetAverageTime(totalTime, interval, elapsedTime)

	if averageTime != 3e+09 {
		t.Errorf("The average was incorrect, got: %v, want: %f", averageTime, 3.00)
	}
}
